package com.java.overrideng;

public class Person {
	
	Person()
	{
		
	}
	/*
	 * The subclass overridden method cannot have weaker access than super class method
	 * 
	 * if i will make that method public then in sub method should be public not accessibility less
	 * than public
	 * Accessibility order
	 * public  protected default private
	 *           public void play()
	                    {
		                  System.out.println("person should play");
	                    }
	                    
	  *if we write static method in both(parent and child) then it is not method overriding it is method binding.
	  *you cannot override static method in Java because method overriding is based upon dynamic binding at runtime and 
	  *                    static methods are bonded using static binding at compile time.                  
	 */
	static{
		System.out.println("this is in static block");
	}
	public static void talk()
	{
		System.out.println("java is interestig subject");
	}
	
	public void play()
    {
      System.out.println("person should play");
    }
	protected void read()
	{
		System.out.println("student should read");
	}
	void work()
	{
		System.out.println("employee should work");
	}
	public void gotoschool(int n)
	{
		System.out.println("student should go to school");
	}

}
