package com.java.overrideng;

public class Student extends Person {
	
	/*
	 * Here method must be public
	 * 
	 * The subclass overridden method cannot have weaker access than super class method
	 * 
	 * method signature should be constant i.e we can not change the return type of a method
	 * 
	 * here static method will as per reference if reference will be parent then parent static method will print and if refernce 
	 * 
	 * will be child the child static block will print
	 */
	static
	{
		System.out.println("subclass");
	}
	public static void talk()
	{
		System.out.println("java is interesting subject <subclass>s");
	}
	 public void play()
	{
		System.out.println("Student should play in evening subclass");
	}
   public void gotoschool(float n)
    {
    	System.out.println(n+ "no of student is in school subclass");
    	//return n;
    }
   protected void read(){
    	System.out.println("student should read at lest 3 hours in a day subclass");
    }
   
   public static void main(String [] arg)
   {
	   Person per=new Student();
	   per.play();
	   per.gotoschool(6);
	   per.talk();
	   Student s=new Student();
	   s.gotoschool(8);   
	   s.talk();
   }
}
