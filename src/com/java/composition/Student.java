package com.java.composition;

public final class Student {
	
	final Person student;
	
	Student()
	{
		student =new Person();
	}
	
	public void studentwork()
	{
		student.gotoschool();
		student.read();
	}

}
