package com.java.composition;

public class Person {
	
	Person()
	{
		
	}
	
	void play()
	{
		System.out.println("person should play");
	}
	void run()
	{
		System.out.println("person should run");
	}
	void read()
	{
		System.out.println("student should read");
	}
	void work()
	{
		System.out.println("employee should work");
	}
	void gotoschool()
	{
		System.out.println("student should go to school");
	}

}
