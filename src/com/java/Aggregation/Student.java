package com.java.Aggregation;

public final class Student {
	
	final Person person;
	Student(Person person)
	{
		this.person=person;
	}

	void studentwork()
	{
		person.gotoschool();
		person.work();
	}
}
