package com.java.Collection.Map.SortByValue;

public class Price {

	private String item;
	private int price;
	
	public Price(String item,int price)
	{
		this.item=item;
		this.price=price;
	}
	
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	 @Override
	public String toString() {
		return item+" "+price;
	}
	
	@Override
	public int hashCode() {
		int hashcode=price;
		hashcode +=item.hashCode();
		return hashcode;
	}
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Price)
		{
			Price pr=(Price) obj;
			return( pr.price ==this.price && pr.item.equals(this.item));
		}
		return false;
	}
}
