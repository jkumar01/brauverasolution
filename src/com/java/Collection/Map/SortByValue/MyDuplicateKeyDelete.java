package com.java.Collection.Map.SortByValue;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
public class MyDuplicateKeyDelete {
	
	public static void main(String[] arg)
	{
		Map<Price,Integer> map=new HashMap<Price,Integer>();
	    map.put(new Price("Banana",20),2);
	    map.put(new Price("Apple",60),1);
	    map.put(new Price("Anar",100),3);
	    map.put(new Price("Banana",20),4);
	    
	    //change map into list
	  List<Map.Entry<Price, Integer>> list =new LinkedList<Map.Entry<Price,Integer>>(map.entrySet()); 
	
	
	  //use collections sort method with comparator
	  Collections.sort(list, new sortbyvalue());
	  
	  //add sorted element in hashmap
	  Map<Price, Integer> map2=new LinkedHashMap<Price,Integer>();
	  for(Map.Entry<Price, Integer> entry:list)
		  map2.put(entry.getKey(), entry.getValue());
	  
	  //print the map2 element
	  for(Map.Entry<Price,Integer> ent:map2.entrySet())
	  {
		  System.out.println(ent.getValue());
	  }
	  
	}
}

class sortbyvalue implements Comparator<Map.Entry<Price, Integer>>
{
	@Override
	public int compare(Entry<Price, Integer> o1, Entry<Price, Integer> o2) {
		
		return o1.getValue().compareTo(o2.getValue());
	}
}